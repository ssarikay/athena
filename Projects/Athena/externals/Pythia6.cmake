#
# File specifying the location of Pythia 6 to use.
#

set( PYTHIA6_LCGVERSION 429.2 )
set( PYTHIA6_LCGROOT
   ${LCG_RELEASE_DIR}/MCGenerators/pythia6/${PYTHIA6_LCGVERSION}/${LCG_PLATFORM} )
